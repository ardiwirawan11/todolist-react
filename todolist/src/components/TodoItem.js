import React, { Component } from 'react'

export default class TodoItem extends Component {
    state = {
        text: '',
        editState: false
    }

    getStyle = () => {
        return {
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration: this.props.todo.isComplete ? 'line-through' : 'none',
        }
    };


    onChange = (e) => {
        this.setState({ text: e.target.value })
    }

    editTrig = (e) => {
        e.preventDefault();
        this.setState({
            editState: true
        })
    }

    EditText() {
        return (
            <div>
                <input
                    type="text"
                    name="update"
                    onChange={this.onChange}
                    value={this.state.text}
                />
                <button onClick={() => {
                    this.props.putDataTodo(this.props.todo._id, this.state.text)
                    this.setState({ editState: false })
                }}>Update</button>
            </div>
        )
    }


    render() {
        const { _id, title, isComplete } = this.props.todo;
        return (
            <div style={this.getStyle()}>
                
                    <input
                        type="checkbox"
                        onChange={this.props.markComplete.bind(this, _id)}
                        checked={isComplete ? true : ''}
                    /> {''}
                    {title}
                    <button onClick={this.props.deleteDataTodo.bind(this, _id)} style={{ float: 'right', marginRight: '10px' }}>
                        Hapus
                    </button>
                    <button onClick={this.editTrig} style={{ float: 'right', marginRight: '10px' }}>
                        Edit
                    </button>
                    {this.state.editState && this.EditText()}
               
            </div>
        )
    }
}
