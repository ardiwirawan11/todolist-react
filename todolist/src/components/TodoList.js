import React, { Component } from 'react'
import TodoItem from './TodoItem'
export default class TodoList extends Component {
    render() {
        return this.props.todos.map((todo) => (
            <TodoItem
                key={todo._id}
                todo={todo}
                deleteDataTodo={this.props.deleteDataTodo}
                putDataTodo={this.props.putDataTodo}
                markComplete={this.props.markComplete}
            />
        ))
    }
}
