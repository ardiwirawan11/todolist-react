import React, { Component } from 'react'

export default class InputForm extends Component {
    state = {
        title: ''
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.props.postDataTodo(this.state.title)
        this.setState({ title: '' })
    }

    onChange = (e) => {
        this.setState({ title : e.target.value })
    }

    render() {
        return (
            <div >
                <input 
                    type="text"
                    name="title"
                    placeholder="Tambahkan aktivitas kamu disini"
                    value={this.state.title}
                    onChange={this.onChange}
                />
                <button 
                    type="submit"
                    onClick={this.onSubmit}
                >Add</button>
            </div>
        )
    }
}

