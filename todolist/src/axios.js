const axios = require('axios');
const url   = 'https://btm-rn.herokuapp.com/api/v1/todo'      

// CRUD method for "Read" using promise
getDataTodo = () => {
    axios.get(url).then(res => {
        console.log(res.data)
    }).catch(err => {
        console.log("Data not found", err)
    })
}
postDataTodo = (data) => {
  axios.post(url, {title:data}).then(res => {
      console.log(res.data)
  }).catch(err => {
      console.log("Data not found", err)
  })
}
putDataTodo = (id,data) => {
  axios.put(url+id, {title:data}).then(res => {
      console.log(res.data)
  }).catch(err => {
      console.log("Data not found", err)
  })
}
deleteDataTodo = (id) => {
  axios.delete(url+id).then(res => {
      console.log(res.data)
  }).catch(err => {
      console.log("Data not found", err)
  })
}
// postDataTodo("coba2");
// putDataTodo('/5d5e1b1f5610780017ec71eb', "asd");
deleteDataTodo('/5d5e1b1f5610780017ec71eb');
getDataTodo();