import React, { Component } from 'react'
import Header from './components/Header'
import InputForm from './components/InputForm'
import TodoList from './components/TodoList'
import axios from 'axios'
const url = 'https://btm-rn.herokuapp.com/api/v1/todo'

export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      todos: []
    }
  }

  getData = () => {
    axios.get(url)
      .then(res => this.setState({ todos: res.data.results }))
  }
  postDataTodo = (data) => {
    axios.post(url, {title:data,  isComplete: false})
  }
  deleteDataTodo = (id) => {
    axios.delete(url+'/'+id)
  }
  putDataTodo = (id,data) => {
    axios.put(url+'/'+id, {title:data})
  }
  markComplete = (id) =>{
    this.state.todos.map(todo => {
      if (todo._id === id) {
        axios.put(url+'/'+id,{
          isComplete: !todo.isComplete
        })
      }
      return todo
    })
  }

  componentDidMount() {
    this.getData()
    setInterval(this.getData, 100)
  }


  render() {
    return (
      <div>
        <Header />
        <InputForm postDataTodo={this.postDataTodo}/>
        <TodoList
          todos={this.state.todos}
          deleteDataTodo={this.deleteDataTodo}
          putDataTodo={this.putDataTodo}
          markComplete={this.markComplete} 
        />
      </div>
    )
  }
}
